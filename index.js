const express = require('express');
/* Create an application with expressjs
This creats an application tha uses express and stores it as app
app is our server*/
const app = express();
//port is just a variable to cintain the port number we want to designate to our server
const port = 4000;
//express.json() is a method from express which allow us to handle the streaming of data and automatically parse the 
//incoming JSON from our request's body

//app.use() is used to run a method or another function for our expressjs api.

app.use(express.json());

//mock data
const users = 
[
	{
		username: "onie",
		email: "onie123@gmail.com",
		password: "onini"
	},
	{
		username: "tristan",
		email: "tristan77@gmail.com",
		password: "tantan"
	},
	{
		username: "jasmuel",
		email: "jasmule51@gmail.com",
		password: "jasjas"
	}
]

const items = 
[
	{
		name: "Stick-O",
		price: 70,
		isActive: true
	},
	{
		name: "Doritos",
		price: 150,
		isActive: true
	},
];

//GET METHOD
app.get('/', (req,res) => {
	//Once the reoute is accessed, we can send a response with the use of res.send()
	//res.send() actually combines our writeHead() and end()
	//It is used to send a resposne to the client and ends the response.
	res.send("Hello World from our first ExpressJS API!");
});

app.get('/hello', (req,res) => {
	res.send("Hello from Batch 152");
});

app.get('/users', (req,res) => {
	res.send(users);
});

//POST METHOD
app.post('/users', (req,res) => {
	console.log(req.body);
	const newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password,

	}
	users.push(newUser);
	console.log(users);
	res.send(users);
});

//DELETE METHOD
app.delete('/users', (req,res) => {

	users.pop();
	console.log(users);
	res.send(users);
});

//PUT METHOD
app.put('/users/', function (req, res) {
	//Updating our user will require us to add an input from our client
	users[req.body.index].password = req.body.password;
	console.log(req.body);
  	res.send(users[req.body.index]);
})
app.put('/users/updatePassword/:index', function (req, res) {
	//Updating our user will require us to add an input from our client
	let index = parseInt(req.params.index);
	if(users.length <= index) {
	
  	res.send("Invalid input.");
  	}
  	else {
  		users[index].password = req.body.password;
		console.log(req.body);
  	}
})











//S28-29 Items Activity

//GET METHOD
app.get('/items', (req,res) => {
	res.send(items);
});

app.get('/items', (req,res) => {
	res.send(items);
});

//S28 POST METHOD
app.post('/items', (req,res) => {
	console.log(req.body);
	const newItem = {
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive,
	}
	items.push(newItem);
	console.log(items);
	res.send(items);
});

//DELETE METHOD
app.delete('/items', (req,res) => {

	items.pop();
	console.log(items);
	res.send(items);
});

//PUT METHOD
app.put('/items', function (req, res) {

	//Updating our user will require us to add an input from our client
	items[req.body.index].price = req.body.price; //get the index of items array from the client's index input 

  	res.send(items[req.body.index]); //display single item from the client's index
  	console.log(items);
})

//PUT METHOD using route params
app.put('/items/:index', function (req, res) {
	
	let index = parseInt(req.params.index);

	/*if(type !== "number") {
		res.send("Index number is not a number.");
		} */
		//else {
	if(items.length <= index) {
		res.send("Invalid input.");
		}
		else {
	items[index].price = req.body.price;

  	res.send(items[index]);
  	console.log(items);
  	console.log();
  	}
  	//}
})





//get details of single user
//GET Method request should not have a requst body. It may have headers for additional information.
//We can pass small amout of data somewhere else: Through the URL.
//Route params are values we can pass via the URL.
//This is done specifically to allow us to send small amount of data into our server through the request URL.
//Route parameters can be defined in the endpoint of a route with :parameterName


app.get('/users/getSingleUser/:index', function(req,res) {
	//URL: http:localhost:4000/users/getSingleUser/0
	//req.params is an object that contains the values passed as route params.
	//In req.params, the parameter name given in the route endpoint becomes the property name
	//Our URL is a string
	console.log(req.params);
	/*
		{ index: '0'}
	*/
	//parseInt() the value of req.params.index to convert the value from string to number.
	let index = parseInt(req.params.index);
	console.log(index);

	//send the user with the appropriate index number
	if(index >= users.length) {
		res.send("Invalid.");
	}
	else {
		res.send(users[index]);
	}
});

app.listen(port,()=>console.log(`Server is running at port ${port}`));



//JS Intro Activity
//GET Method - getSingleItem
app.get('/items/getSingleItem/:index', function(req,res) {	
	let index = parseInt(req.params.index);
	
		res.send(items[index]);
});

//PUT Method - Archive Single Item
app.put('/items/archive/:index', function(req,res) {	
	let index = parseInt(req.params.index);

	items[index].isActive = false;
	res.send(items[index]);
});

//PUT Method - Activate Single Item
app.put('/items/activate/:index', function(req,res) {	
	let index = parseInt(req.params.index);

	items[index].isActive = true;
	res.send(items[index]);
});